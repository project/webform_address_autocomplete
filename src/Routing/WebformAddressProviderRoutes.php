<?php

namespace Drupal\webform_address_autocomplete\Routing;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Provides dynamic routes for WebformAddressProvider.
 */
class WebformAddressProviderRoutes implements ContainerInjectionInterface {

  /**
   * The Webform Address Provider manager.
   *
   * @var \Drupal\webform_address_autocomplete\Plugin\WebformAddressProviderManager
   */
  protected $providerManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(PluginManagerInterface $provider_manager) {
    $this->providerManager = $provider_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('plugin.manager.webform_address_provider')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function routes() {
    $pluginDefinitions = $this->providerManager->getDefinitions();

    $routes = [];

    foreach ($pluginDefinitions as $id => $pluginDefinition) {
      $pluginUrl = str_replace('_', '-', $pluginDefinition['id']);
      $routes["webform_address_autocomplete.webform_address_provider.$id"] = new Route(
        "admin/config/webform-address-autocomplete/$pluginUrl",
        [
          '_form' => '\Drupal\webform_address_autocomplete\Form\ProviderSettingsForm',
          '_title' => 'Configure ' . $pluginDefinition['label'],
          '_plugin_id' => $id,
        ],
        [
          '_permission' => 'access administration pages',
        ]
      );
    }

    return $routes;
  }

}
