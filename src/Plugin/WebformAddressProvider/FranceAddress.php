<?php

namespace Drupal\webform_address_autocomplete\Plugin\WebformAddressProvider;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\webform_address_autocomplete\Plugin\WebformAddressProviderBase;

/**
 * Defines a FranceAddress plugin for webform_address_autocomplete.
 *
 * @WebformAddressProvider(
 *   id = "france_address",
 *   label = @Translation("France Address"),
 * )
 */
class FranceAddress extends WebformAddressProviderBase {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return array_merge(parent::defaultConfiguration(), [
      'endpoint' => 'https://api-adresse.data.gouv.fr/search/',
      'type' => '',
      'postcode' => NULL,
      'citycode' => NULL,
      'lat' => NULL,
      'lon' => NULL,
      'limit' => 10,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['endpoint'] = [
      '#type' => 'url',
      '#title' => $this->t('Data API Address endpoint'),
      '#default_value' => $this->configuration['endpoint'],
      '#description' => $this->t('More information <a href="@api">@apiLabel</a>', [
        '@api' => 'https://api.gouv.fr/les-api/base-adresse-nationale',
        '@apiLabel' => 'Base Adresse Nationale',
      ]),
    ];
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Query type'),
      '#options' => [
        '' => $this->t('Default'),
        'housenumber' => $this->t('House number'),
        'street' => $this->t('Street'),
        'locality' => $this->t('Locality'),
        'municipality' => $this->t('Municipality'),
      ],
      '#empty_option' => $this->t('Default'),
      '#empty_value' => '',
      '#default_value' => $this->configuration['type'],
    ];
    $form['postcode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal code'),
      '#default_value' => $this->configuration['postcode'],
      '#placeholder' => 'Ex: 33000',
    ];
    $form['citycode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City code'),
      '#default_value' => $this->configuration['citycode'],
      '#description' => $this->t('Code INSEE'),
      '#placeholder' => 'Ex: 33063',
    ];
    $form['lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latitude'),
      '#default_value' => $this->configuration['lat'],
      '#placeholder' => 'Ex: 44.83761',
    ];
    $form['lon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Longitude'),
      '#default_value' => $this->configuration['lon'],
      '#placeholder' => 'Ex: -0.57656',
    ];
    $form['limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit'),
      '#default_value' => $this->configuration['limit'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $configuration['endpoint'] = $form_state->getValue('endpoint');
    $configuration['type'] = $form_state->getValue('type');
    $configuration['postcode'] = $form_state->getValue('postcode');
    $configuration['citycode'] = $form_state->getValue('citycode');
    $configuration['lat'] = $form_state->getValue('lat');
    $configuration['lon'] = $form_state->getValue('lon');
    $configuration['limit'] = $form_state->getValue('limit');
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritDoc}
   */
  public function processQuery($string) {
    $results = [];

    $exploded_query = explode('||', $string);
    $q = trim($exploded_query[0]);

    // France Address requires at least 3 non-spaces characters for query.
    if ((mb_strlen($q) < 3) || !preg_match('/^[a-zA-Z0-9]/', $q)) {
      return $results;
    }

    $query = [
      'autocomplete' => 0,
      'limit' => $this->configuration['limit'] ?: 10,
      'q' => $q,
    ];

    $type = $this->configuration['type'];
    if (!empty($type)) {
      $query['type'] = $type;
    }
    $postcode = $this->configuration['postcode'];
    if (!empty($postcode)) {
      $query['postcode'] = $postcode;
    }
    $citycode = $this->configuration['citycode'];
    if (!empty($citycode)) {
      $query['citycode'] = $citycode;
    }
    $lat = $this->configuration['lat'];
    $lon = $this->configuration['lon'];
    if (!empty($lat) && !empty($lon)) {
      $query['lat'] = $lat;
      $query['lon'] = $lon;
    }

    $url = $this->configuration['endpoint'] . '?' . http_build_query($query);

    try {
      $response = $this->client->request('GET', $url);
      $content = Json::decode($response->getBody());
      if (!empty($content['features'])) {
        foreach ($content['features'] as $key => $feature) {
          if (!empty($feature['properties'])) {
            $results[$key]['street_name'] = $feature['properties']['name'];
            $results[$key]['town_name'] = $feature['properties']['city'];
            $results[$key]['zip_code'] = $feature['properties']['postcode'];
            $results[$key]['label'] = $feature['properties']['label'];
          }
          if (!empty($feature['geometry'])) {
            $results[$key]['location'] = [
              'longitude' => $feature['geometry']['coordinates'][0],
              'latitude' => $feature['geometry']['coordinates'][1],
            ];
          }
        }
      }
    }
    catch (\Exception $e) {
      Error::logException($this->getLogger('webform_address_autocomplete'), $e);
    }

    return $results;
  }

}
