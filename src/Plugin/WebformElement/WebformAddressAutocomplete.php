<?php

namespace Drupal\webform_address_autocomplete\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\WebformAddress;

/**
 * Provides a 'address autocomplete' element.
 *
 * @WebformElement(
 *   id = "webform_address_autocomplete",
 *   label = @Translation("Address autocomplete"),
 *   description = @Translation("Provides a address field element with auto completion."),
 *   category = @Translation("Advanced elements"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 */
class WebformAddressAutocomplete extends WebformAddress {
}
