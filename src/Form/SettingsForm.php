<?php

namespace Drupal\webform_address_autocomplete\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the webform address autocomplete module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Webform Address Provider manager.
   *
   * @var \Drupal\webform_address_autocomplete\Plugin\WebformAddressProviderManager
   */
  protected $providersManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->providersManager = $container->get('plugin.manager.webform_address_provider');
    return $instance;
  }

  /**
   * Name of the config.
   *
   * @var string
   */
  public static $configName = 'webform_address_autocomplete.settings';

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [SettingsForm::$configName];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'webform_address_autocomplete_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(SettingsForm::$configName);

    $header = [
      'provider' => $this->t('Provider'),
      'operations' => $this->t('Operations'),
    ];

    $pluginDefinitions = $this->providersManager->getDefinitions();

    $options = [];

    foreach ($pluginDefinitions as $id => $pluginDefinition) {
      $options[$pluginDefinition['id']] = [
        'provider' => $pluginDefinition['label'],
        'operations' => [
          'data' => [
            '#type' => 'operations',
            '#links' => [
              'settings' => [
                'title' => $this->t('Settings'),
                'url' => URL::fromRoute("webform_address_autocomplete.webform_address_provider.$id"),
              ],
            ],
          ],
        ],
      ];
    }

    $form['active_plugin'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#multiple' => FALSE,
      '#default_value' => $config->get('active_plugin') ? $config->get('active_plugin') : NULL,
      '#empty' => $this->t('No plugins found'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(SettingsForm::$configName);
    $config->set('active_plugin', $form_state->getValue(['active_plugin']));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
