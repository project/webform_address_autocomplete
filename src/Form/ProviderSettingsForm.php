<?php

namespace Drupal\webform_address_autocomplete\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the address provider.
 */
class ProviderSettingsForm extends ConfigFormBase {

  /**
   * Name of the config.
   *
   * @var string
   */
  public static $configName = 'webform_address_autocomplete.settings';

  /**
   * The Webform Address Provider manager.
   *
   * @var \Drupal\webform_address_autocomplete\Plugin\WebformAddressProviderManager
   */
  protected $providerManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->providerManager = $container->get('plugin.manager.webform_address_provider');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [ProviderSettingsForm::$configName];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'webform_address_autocomplete_settings_form_' . $this->getPluginIdFromRequest();
  }

  /**
   * {@inheritDoc}
   */
  protected function getPluginIdFromRequest() {
    $request = $this->getRequest();
    return $request->get('_plugin_id');
  }

  /**
   * {@inheritDoc}
   */
  public function getPluginInstance($plugin_id) {
    return $this->providerManager->createInstance($plugin_id);
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginIdFromRequest();
    $instance = $this->getPluginInstance($plugin_id);
    $form = $instance->buildConfigurationForm($form, $form_state);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginIdFromRequest();
    $instance = $this->getPluginInstance($plugin_id);
    $instance->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $plugin_id = $this->getPluginIdFromRequest();
    $instance = $this->getPluginInstance($plugin_id);
    $instance->submitConfigurationForm($form, $form_state);
    $config = $this->config(ProviderSettingsForm::$configName);
    $instanceConfiguration = $instance->getConfiguration();
    $config->set($plugin_id, serialize($instanceConfiguration));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
