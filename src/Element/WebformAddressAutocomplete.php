<?php

namespace Drupal\webform_address_autocomplete\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Element\WebformAddress;
use Drupal\webform_address_autocomplete\Form\SettingsForm;

/**
 * Provides a webform element for an address with autocomplete.
 *
 * @FormElement("webform_address_autocomplete")
 */
class WebformAddressAutocomplete extends WebformAddress {

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();

    $info['#process'][] = [
      get_class($this),
      'processAutocomplete',
    ];

    return $info;
  }

  /**
   * {@inheritDoc}
   */
  public static function processAutocomplete(&$element, FormStateInterface $form_state, &$complete_form) {

    $element['address']['#attributes']['class'][] = 'webform_address_autocomplete';

    $config = \Drupal::configFactory()->get(SettingsForm::$configName);
    if (!$config->get('active_plugin')) {
      $element['message'] = [
        '#type' => 'item',
        '#markup' => t('Address autocomplete provider isn\'t selected. You can do it <a href="@url">here</a>.', [
          '@url' => Url::fromRoute('webform_address_autocomplete.settings')
            ->toString(),
        ]),
        '#wrapper_attributes' => [
          'class' => ['messages', 'messages--warning'],
        ],
        '#weight' => -10,
      ];
    }

    $element['#attached']['library'][] = 'webform_address_autocomplete/webform_address_autocomplete';
    $element['address']['#autocomplete_route_name'] = 'webform_address_autocomplete.addresses';
    $element['address']["#attributes"]['placeholder'] = t('Please start typing your address...');
    $country_code = !empty($element["#default_value"]["country_code"]) ? $element['#default_value']['country_code'] : NULL;
    if (!empty($element['#value']['country_code'])) {
      $country_code = $element['#value']['country_code'];
    }
    $element['address']['#autocomplete_route_parameters'] = ['country' => $country_code];
    return $element;
  }

}
