<?php

namespace Drupal\webform_address_autocomplete\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Webform Address provider item annotation object.
 *
 * @see \Drupal\webform_address_autocomplete\Plugin\WebformAddressProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class WebformAddressProvider extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
