<?php

namespace Drupal\webform_address_autocomplete\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webform_address_autocomplete\Form\SettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for watches autocomplete form elements.
 *
 * @todo try and restrict direct access somehow... Drupal's CSRF token does not
 *   work with Anonymous user... Maybe custom CSRF token / custom check?
 */
class WebformAddressAutocomplete extends ControllerBase {

  /**
   * The webform_address_autocomplete module settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The Webform Address Provider manager.
   *
   * @var \Drupal\webform_address_autocomplete\Plugin\WebformAddressProviderManager
   */
  protected $providerManager;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->config = $container->get('config.factory')->get(SettingsForm::$configName);
    $instance->providerManager = $container->get('plugin.manager.webform_address_provider');
    return $instance;
  }

  /**
   * Handler for the autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $input = trim($request->query->get('q'), '"');
    if (!empty($request->query->get('country'))) {
      $input .= '||' . $request->query->get('country');
    }
    $results = $this->getProviderResults($input);
    return new JsonResponse($results);
  }

  /**
   * {@inheritDoc}
   */
  public function getProviderResults($string) {
    $plugin_id = $this->config->get('active_plugin');
    $plugin = $this->providerManager->createInstance($plugin_id);
    return $plugin->processQuery($string);
  }

}
