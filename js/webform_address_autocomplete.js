(($, Drupal, once) => {
  Drupal.behaviors.webform_address_autocomplete = {
    attach(context) {
      $(
        once(
          'initiate-autocomplete',
          'input.webform_address_autocomplete',
          context,
        ),
      ).each(function processElement() {
        const formWrapper = $(this).closest('.js-form-wrapper');
        const uiAutocomplete = $(this).data('ui-autocomplete');
        const drupalSelector = formWrapper.data('drupal-selector');

        uiAutocomplete.options.select = (event, ui) => {
          event.preventDefault();
          formWrapper
            .find(`input[data-drupal-selector=${drupalSelector}-address]`)
            .val(ui.item.street_name);
          formWrapper
            .find(`input[data-drupal-selector=${drupalSelector}-postal-code]`)
            .val(ui.item.zip_code);
          formWrapper
            .find(`input[data-drupal-selector=${drupalSelector}-city]`)
            .val(ui.item.town_name);
          formWrapper
            .find(
              `select[data-drupal-selector=${drupalSelector}-administrative-area], input[data-drupal-selector=${drupalSelector}-administrative-area]`,
            )
            .val(ui.item.administrative_area);
        };
      });
    },
  };
})(jQuery, Drupal, once);
