# Webform Address Autocomplete

This module enhances the standard Webform Address element with autocomplete
functionality.

This module is mainly based on the [address_autocomplete](https://www.drupal.org/project/address_autocomplete)
module.

When you start to type the street, a list of matching addresses will be shown.
When you pick one, all relevant address fields (like street, city, zip code,
country) will be populated.

Multiple provider can be configured.

Currently available:

- [France Base Adresse](https://adresse.data.gouv.fr/api-doc/adresse)
- [Swiss Post API](https://developer.post.ch/en/address-web-services-rest)
- [Google Maps](https://developers.google.com/maps/documentation/geocoding/start)
- [Mapbox Geocoding](https://docs.mapbox.com/api/search/geocoding/)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/webform_address_autocomplete).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/webform_address_autocomplete).

## Features

- Extends the default Webform Address element with autocomplete functionality.
- Contains an extendable AddressProvider plugin system, so it's possible to add
  new Provider plugins easily & switch between them.


## Requirements

This module requires the [Webform](https://www.drupal.org/project/webform) module.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Go to `/admin/config/webform-address-autocomplete` to configure your address
provider.
3. Enable the provider you want to use, configure it by clicking on the
Settings button.
4. Add an **Address autocomplete** element to your webform.

### Address elements ordering

You can change the order of the address elements using the address element
**Custom properties** field (**Advanced** tab):

```
address__weight: 10
address_2__weight: 20
postal_code__weight: 30
city__weight: 40
state_province__weight: 50
country__weight: 60
```

### Address elements autocomplete

You can define the `autocomplete` value of each address elements using the
address element **Custom properties** field (**Advanced** tab):

```
address__autocomplete: address-line1
address_2__autocomplete: address-line2
postal_code__autocomplete: postal-code
city__autocomplete: address-level2
state_province__autocomplete: address-level1
country__autocomplete: country-name
```

## Developers

If you would like to add a new API provider, simply extend
`WebformAddressProviderBase`, and add your API request implementation.
Remember to share it with the community.

## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
